package org.huseyin;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Hello world!
 *
 */
public class App {
    
    public static void main(String[] args) {
        System.out.println("Hello MongoDB World!");
        
        try {
            MongoClient mongo = new MongoClient("localhost", 27017);

            // ----------------- LIST DBs -------------------//
            System.out.println("List of MongoDB databases:");
            listDbs(mongo);

            // Get database. If the database doesn�t exist it will be created.
            DB db = mongo.getDB("testdb");
            DBCollection userCollection = db.getCollection("user");

            // ----------------- CREATE ---------------------//
            System.out.println("Create a record:");
            create(userCollection);
            createWithBuilder(userCollection);
            createWithMap(userCollection);
            createWithJSON(userCollection);
            read(userCollection);
            //------------------ UPDATE ---------------------//
            System.out.println("Update Records:");
            update(userCollection);
            read(userCollection);
            //-------------------DELETE----------------------//
            System.out.println("Delete Records:");
            delete(userCollection);
            read(userCollection);
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void create(DBCollection collection) {
        BasicDBObject document = new BasicDBObject();
        document.put("name", "huseyin");
        document.put("age", 32);
        document.put("createdAt", new Date());
        collection.insert(document);
    }
    
    private static void createWithBuilder(DBCollection collection) {
        BasicDBObjectBuilder doc = BasicDBObjectBuilder.start()
                .add("name", "huseyin")
                .add("age", 32)
                .add("createdAt", new Date());
        collection.insert(doc.get());
    }
    
    private static void createWithMap(DBCollection collection) {
        Map<String, Object> doc = new HashMap<String, Object>();
        doc.put("name", "huseyin");
        doc.put("age", 32);
        doc.put("createdAt", new Date());
        
        collection.insert(new BasicDBObject(doc));
    }
    
    private static void createWithJSON(DBCollection collection) {
        String json = "{'name' : 'huseyin','age' : 32, 'createdAt' : { '$date' : '2014-07-30T10:13:50.198Z'}}";
        DBObject dbObject = (DBObject) JSON.parse(json);
        collection.insert(dbObject);
    }
    
    private static void read(DBCollection collection) {
        BasicDBObject query = new BasicDBObject();
        query.put("name", "huseyin");
        DBCursor cursor = collection.find(query);
        
        System.out.println(cursor.count() + " record found");
        
        while (cursor.hasNext()) {
            System.out.println(cursor.next());
        }
    }
    
    private static void update(DBCollection collection) {
        BasicDBObject query = new BasicDBObject();
        query.put("name", "huseyin");
        
        BasicDBObject newDoc = new BasicDBObject();
        newDoc.put("age", 33);
        
        BasicDBObject updatedDoc = new BasicDBObject();
        updatedDoc.put("$set", newDoc);
        collection.update(query, updatedDoc);
    }
    
    private static void delete(DBCollection collection) {
        BasicDBObject query = new BasicDBObject();
        query.put("name", "huseyin");
        
        collection.remove(query);
    }
    
    private static void listDbs(MongoClient mongo) {
        List<String> dbs = mongo.getDatabaseNames();
        for (String db : dbs) {
            System.out.println(" - " + db);
        }
    }
}
